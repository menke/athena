/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page TrigBphysHypo_page TrigBphysHypo Package
@author Julie.Kirk@rl.ac.uk, John.Baines@rl.ac.uk, Natalia.Panikashvili@cern.ch, Serguei.Sivoklokov@cern.ch

@section TrigBphysHypo_TrigBphysHypoIntro Introduction
This package contains the code for hypothesis algorithms in the Bphysics slice of the trigger. 
It comprises the following algorithms:

TrigL2BMuMuFex.cxx and TrigL2BMuMuHypo.cxx : Select B->mumu decays

TrigL2BMuMuXFex.cxx and TrigL2BMuMuXHypo.cxx : Select B->mumuX decays

TrigL2DsPhiPiFex.cxx, TrigL2DsPhiPiHypo.cxx, TrigEFDsPhiPiFex.cxx, TrigEFDsPhiPiHypo.cxx :    Select B decays with subsequent Ds->PhiPi

TrigL2JpsieeFex.cxx and TrigL2JpsieeHypo.cxx :      Select B decays with Jpsi->ee

JpsiHypo.cxx  :                   Select Jpsi->mumu decays after TrigDiMuon from MuonHypo package.



*/
